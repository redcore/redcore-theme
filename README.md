# Redcore-theme
	best to be viewed togheter with Numix-Circle icon theme
	https://github.com/numixproject/numix-icon-theme-circle

#### openbox
	added 7/28/2016 for LXQt edition

#### metacity-2
	updated 4/27/2016
	designed to automatically pick-up gtk colors

#### xfwm
	updated 4/2/2016 - cleaner look
	designed to automatically pick-up gtk colors

#### gtk2
	re-worked based on Numix GTK theme
	https://github.com/numixproject/numix-gtk-theme

#### gtk3
	done for now, fixes may be needed
	based on Numix GTK theme

#### cinnamon
	lost in a galaxy far-far away
